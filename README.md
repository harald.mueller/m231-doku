# m231-doku - Meine Doku zum Modul m231
(Datenschutz und Datensicherheit anwenden)


[Moduldefinition gemäss Modulbaukasten](https://www.modulbaukasten.ch/module/231/1/de-DE?title=Datenschutz-und-Datensicherheit-anwenden)


## Tag 1: Meine Daten
("Wo sind meine Daten", "Dateiablagekonzept")

## Tag 2: Git
("Ablage von Projektdaten in einem Repository")

## Tag 3: Git + Daten, Fortsetzung


## Tag 4: Datenschutz 
("Gesetzeslage verstehen")

<br>

<hr>

August, harald.mueller@bluewin.ch
